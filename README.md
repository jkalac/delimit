# delimit

simple console log ...: delimited output

## Usage

```js
var delimit =  require('delimit')
if (DEBUG) console.log(delimit('Some'), 'Info') // Some..............: Info
```

## License

Private
