'use strict'

var lib    = require('./')
var assert = require('assert')

var test   = lib('hello')
assert.equal(test, 'hello....................:', 'Unexpected')

var test   = lib('hello', 2)
assert.equal(test, 'he:', 'Unexpected')
