'use strict';

var S          = require('string')
var delimitLen = process.env.DELIMIT_LEN || 25
function delimit(str, len) {
  len = len || delimitLen
  str = str || ''
  if (str.length > len) str = str.substr(0, len)
  return str + S('.').repeat(len - str.length).s + ':'
}

module.exports = delimit
